package com.services.springboot.stsgitproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StsgitprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(StsgitprojectApplication.class, args);
	}

}
